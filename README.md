# 开发环境
`开发环境的搭建可以到本人[博客](https://www.devicemanger.cn/posts/8839.html)查看`
## 硬件环境
* 原子的`stm32f767`阿波罗开发板
* stlink烧录器

## 软件环境
* windwos的子系统采用wsl-2
* windows下的linux子系统版本`Ubuntu18.04TLS`
* zephyr版本`v2.4.0-rc1`
* zephyr的工具链`v0.11.4`
* cmake版本`3.18.2`
* west版本`v0.7.3`
* python版本`3.6.9`
* openocd版本`0.10.0`,这里一定要在windows下安装，不要在子系统下安装

# 工程的搭建
## 前提条件
`以下这些前提条件的安装都在博客上有介绍`
* 安装好了windows的linux子系统
* 安装好了`zephyr`的依赖
* 安装好了`python`
* 安装好了`west`
* 安装好了`zephyr的工具链`

## 让此应用跑起来
* 在linux下新建一个文件夹，当作工程目录
```shell
mkdir ~/stm32f7-demo
```
* 进入工程目录，克隆代码
```shell
cd ~/stm32f7-demo
git clone https://gitee.com/wangyan1003/stm32f767-zephyr.git
```
* 初始化工程目录，要在`工程目录`下执行
```shell
west init -l stm32f767-zephyr
```
* 下载工程需要的模块,在`工程目录`下执行
```shell
west update
```
* 安装zephyr的python依赖
```shell
pip3 install -r zephyr/scripts/requirements.txt
```
* 安装mcuboot的python依赖
```shell
pip3 install -r bootloader/mcuboot/scripts/requirements.txt
```
* 安装`imgtool`
```shell
pip3 install imgtool
```
* 将应用目录下的mcuboot.conf中的`CONFIG_BOOT_SIGNATURE_KEY_FILE`属性设置为应用目录下的`keys/root-rsa-2048.pem`文件的`绝对路径`
* 编译编译代码。将开发板上电，链接stlink烧录器，然后在`应用目录`下执行
```shell
west relase -c -f
```

# 此项目目前添加的功能
## 自定义`west`指令
### stlink指令
这个指令主要是单纯的通过`stlink`烧录器去下载程序的
格式：
```shell
west stlink [options]
```
`以下指令可以组合使用`
* 指定文件名，这个是必填的
```shell
west stlink -n <name>
```
* 指定烧录地址,默认是`0x8000000`
```shell
west stlink -a <address>
```

### relase指令
这个指令主要是为了完成一键`构建+烧录`而创建。
格式
```shell
west relase [options]
```
* 需要编译生成的文件名,默认为`stm32f767.bin`。文件会存放在`应用程序目录`下`out`文件下
```shell
west relase -n <name>
```
* 需要下载的文件地址,默认为`0x8000000`
```shell
west relase -a <address>
```
* 是否要下载,默认是不下载只编译
```shell
west relase -f
```
* 在编译之前是否需要完全清理工程,默认是不完全清理的
```shell
west relase -c
```
* 对二进制文件进行验签需要用到的key文件，默认文件为`root-rsa-2048.pem`。这里要注意验签文件只能放在`应用目录`下的`keys`文件夹下
```shell
west relase -k <pemfile>
```
* 指定boot二进制文件的大小(这里其实是二进制文件的偏移并不是实际大小，但用大小可能更好理解)，以k为单位。默认为`128k`
```shell
west relase -o <boot-offsize>
```

## 系统功能
* mcuboot添加
* logger添加,对用shell指令`testlog`
* shell添加

## 驱动添加
* uart驱动,对应shell指令`uart`
* pcf8754驱动,对应shell指令`pcf8574`
* led驱动,对应shell指令`led`
* 按键驱动,对应shell指令`key`
* eeprom驱动,对应shell指令`eeprom_iic`
* ap3216c驱动,对应shell指令`ap3216c`
* 看门狗驱动,对应shell指令`iwdg`

## 内核服务
* 线程服务,对应shell指令`thread`

# 开发遇到的问题
## 问题1
如果出现以下这个警告，其实是因为我们的板子信息是建立在应用目录下的：
```
WARNING: This looks like a fresh build and BOARD is unknown; so it probably won't work. To fix, use --board=<your-board>.
```
执行以下就可以去掉这个警告
```shell
west config build.board_warn false
```

# 个人博客
https://www.devicemanger.cn

# 个人联系方式
邮箱: 18501916597@163.com
QQ:   2649223687
微信: FengYingYueDing