#include "stm32f7_pcf8574.h"
#include <drivers/i2c.h>

//0成功
static int iic_write(const struct device *device,const uint8_t *buf, uint32_t num_bytes, uint16_t addr,uint16_t timeout)
{
	int err;
	int64_t out;
	out = k_uptime_get() + timeout;
	while (1) {
		int64_t now = k_uptime_get();
		err = i2c_write(device, (const uint8_t *)buf, num_bytes,addr);
		if (!err || now > out) {
			break;
		}
		k_sleep(K_MSEC(1));
	}
	if (err < 0) {
		return err;
	}
	return 0;
}

//0成功
static int iic_read(const struct device *device,uint8_t *buf, uint32_t num_bytes, uint16_t addr,uint16_t timeout)
{
	int err;
	int64_t out;
	out = k_uptime_get() + timeout;
	while (1) {
		int64_t now = k_uptime_get();
		err=i2c_read(device, buf, num_bytes,addr);
		if (!err || now > out) {
			break;
		}
		k_sleep(K_MSEC(1));
	}
	if (err < 0) {
		return err;
	}
	return 0;
}
//向位写入状态 0:代表成功
static int pcf8574_writebit(const struct device *device,uint8_t bit,uint8_t sta)
{
	int err;
	const struct pcf8574_driver_config *cfg = device->config;
	const struct pcf8574_driver_data *data = device->data;
	uint8_t tmp;
	if(data->bus_device!=NULL){
		err=iic_read(data->bus_device,&tmp,1,cfg->address,cfg->timeout);
		if(err){
			printk("err1:%d\n",err);
			return -1;
		}
		if(sta==NO_ACTIVE)
			tmp&=~(1<<bit);     
    	else 
			tmp|=1<<bit;
		err=iic_write(data->bus_device,&tmp,1,cfg->address,cfg->timeout);
		if(err){
			printk("err2:%d\n",err);
			return -1;
		}
	}else{
		printk("err: dev is null\n");
		return -1;
	}
	
	return 0;
}
//从位读取状态 >=0代表成功 -1:代表失败
static int pcf8574_readbit(const struct device *device,uint8_t bit)
{
	int err;
	const struct pcf8574_driver_config *cfg = device->config;
	const struct pcf8574_driver_data *data = device->data;
	uint8_t tmp;
	if(data->bus_device!=NULL){
		err=iic_read(data->bus_device,&tmp,1,cfg->address,cfg->timeout);
		if(err){
			return -1;
		}
	}else{
		return -1;
	}
	if(tmp&(1<<bit)){
		return 1;
	}
	else{
		return 0; 
	}	
}

static const struct pcf8574_driver_api pcf8574_stm32_driver = {
	.writebit=pcf8574_writebit,
    .readbit=pcf8574_readbit,
};

//pcf8574的初始化
static int pcf8574_init(const struct device *device)
{
	int err;
	uint8_t tmp=0xff;
	const struct pcf8574_driver_config *cfg = device->config;
	struct pcf8574_driver_data *data = device->data;
	if((data!=NULL)&&(cfg!=NULL)){
		//获取gpio
		data->gpio_device = device_get_binding(data->gpio_name);
		if(data->gpio_device){
			//配置GPIO为输入,且为高电平,低电平时
			err = gpio_pin_configure(data->gpio_device, data->pin, GPIO_INPUT | GPIO_INT_DEBOUNCE|GPIO_PULL_UP|GPIO_OUTPUT_HIGH);
			if(err){
                return -1;
            }
		}else{
			return -1;
		}
		//获取iic的控制器驱动
		data->bus_device = device_get_binding(cfg->name);
		if(data->bus_device==NULL){
			return -1;
		}
		iic_write(data->bus_device,&tmp,1,cfg->address,cfg->timeout);
	}else{
		return -1;
	}
	return 0;
}


#define PCF8574_DEVICE_INIT(__name, __bus, __base_name, __port,__address,__timeout) 				\
	static const struct pcf8574_driver_config pcf8574_stm32_cfg = {   	    \
        .name=__bus,                                                        \
        .address=__address,                                                 \
		.timeout=__timeout,													\
	};								       									\
	static struct pcf8574_driver_data pcf8574_stm32_data = {			    \
    	.name = __name,														\
		.bus_device = NULL,													\
        .gpio_name=__base_name,                                             \
        .pin=__port,                                                       \
		.gpio_device = NULL,												\
	};	       																\
	DEVICE_AND_API_INIT(pcf8574_stm32,			       \
			    __name,					       \
			    pcf8574_init,				       \
			    &pcf8574_stm32_data,		       \
			    &pcf8574_stm32_cfg,		       \
			    POST_KERNEL,				       \
			    CONFIG_APPLICATION_INIT_PRIORITY,	       \
			    &pcf8574_stm32_driver)

#define PCF8574_DEVICE_INIT_STM32(__name)						\
	PCF8574_DEVICE_INIT(DT_LABEL(DT_NODELABEL(__name)),	                            \
			 DT_LABEL(DT_BUS(DT_NODELABEL(__name))),					                \
			 DT_LABEL(DT_PHANDLE(DT_NODELABEL(__name),gpios)),		\
			 DT_PHA_BY_IDX(DT_NODELABEL(__name), gpios, 0, pin),    \
             DT_REG_ADDR(DT_NODELABEL(__name)),						\
			 DT_PROP(DT_NODELABEL(__name), timeout))

#if DT_NODE_EXISTS(DT_NODELABEL(pcf8574))
PCF8574_DEVICE_INIT_STM32(pcf8574);
#endif