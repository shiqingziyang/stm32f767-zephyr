#ifndef _APP_STM32F7_PCF8574_H_
#define _APP_STM32F7_PCF8574_H_
#include<zephyr.h>
#include <device.h>
#include <drivers/gpio.h>
// 扩展的引脚偏移
#define PCF8574_BEEP                (uint8_t)(0)
#define PCF8574_AP_INT              (uint8_t)(1)
#define PCF8574_DCMI_PWDN           (uint8_t)(2)
#define PCF8574_USB_PWR             (uint8_t)(3)
#define PCF8574_EXIO                (uint8_t)(4)
#define PCF8574_INT_9D              (uint8_t)(5)
#define PCF8574_RS485_REG           (uint8_t)(6)
#define PCF8574_ETH_RESET           (uint8_t)(7)

// 引脚状态
#define ACTIVE      1
#define NO_ACTIVE   0

/**
 * pcf8574的api结构
 */ 
struct pcf8574_driver_api {
    //向位写入状态
    int (*writebit)(const struct device *dev,uint8_t bit,uint8_t sta);
    //从位读取状态
    int (*readbit)(const struct device *dev,uint8_t bit);
};

/**
 * pcf8574的config结构
 */ 
struct pcf8574_driver_config {
    //IIC的地址
    uint16_t address;
    //bus的label名字
    uint8_t *name;
    //IIC操作超时时间
    uint16_t timeout;
};

/**
 * pcf8574的data结构
 */ 
struct pcf8574_driver_data {
    //中断gpio的状态
	const struct device *gpio_device;
    //gpio的label名字
    uint8_t *gpio_name;
    //gpio的port
    uint32_t pin;
    //gpio中断的回调
    struct gpio_callback gpio_cb;
    //驱动的名字
	uint8_t *name;
    //bus的状态
	const struct device *bus_device;
};

#endif