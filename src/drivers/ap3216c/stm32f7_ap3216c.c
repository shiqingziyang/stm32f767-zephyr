#include "stm32f7_ap3216c.h"
#include <drivers/i2c.h>
#include <kernel.h>
#include <stdlib.h>
#include <string.h>
#include <drivers/sensor.h>
#include <sys/__assert.h>

static int ap3216c_reg_read(const struct device *dev, uint8_t reg, uint8_t *val)
{
	struct ap3216c_driver_data *drv_data = dev->data;
	const struct ap3216c_driver_config *cfg = dev->config;
    int err;
    uint16_t timeout = cfg->timeout;
    int64_t out;
	out = k_uptime_get() + timeout;
    while(1){
        int64_t now = k_uptime_get();
        err=i2c_reg_read_byte(drv_data->bus_device, cfg->address,reg, val);
        if (!err || now > out) {
			break;
		}
        k_sleep(K_MSEC(1));
    }
    if (err < 0) {
		return err;
	}
	return 0;
}

static int ap3216c_reg_write(const struct device *dev, uint8_t reg, uint8_t val)
{
	struct ap3216c_driver_data *drv_data = dev->data;
	const struct ap3216c_driver_config *cfg = dev->config;
    int err;
    uint16_t timeout = cfg->timeout;
    int64_t out;
	out = k_uptime_get() + timeout;
    while(1){
        int64_t now = k_uptime_get();
        err=i2c_reg_write_byte(drv_data->bus_device, cfg->address,reg, val);
        if (!err || now > out) {
			break;
		}
        k_sleep(K_MSEC(1));
    }
    if (err < 0) {
		return err;
	}
	return 0;
}

static int ap3216c_asl_reg_read(const struct device *dev, uint8_t *val)
{
    uint8_t err;
    
    err=ap3216c_reg_read(dev,AP3216C_ALS_LOW_REG,&val[0]);
    if(err){
        return -EIO;
    }
    k_sleep(K_MSEC(5));
    err=ap3216c_reg_read(dev,AP3216C_ALS_HIGH_REG,&val[1]);
    if(err){
        return -EIO;
    }
    return 0;
}

static int ap3216c_ir_reg_read(const struct device *dev, uint8_t *val)
{
    uint8_t err;
    err=ap3216c_reg_read(dev,AP3216C_IR_LOW_REG,&val[0]);
    if(err){
        return -EIO;
    }
    k_sleep(K_MSEC(5));
    err=ap3216c_reg_read(dev,AP3216C_IR_HIGH_REG,&val[1]);
    if(err){
        return -EIO;
    }
    return 0;
}

static int ap3216c_ps_reg_read(const struct device *dev, uint8_t *val)
{
    uint8_t err;
    
    err=ap3216c_reg_read(dev,AP3216C_PS_LOW_REG,&val[0]);
    if(err){
        return -EIO;
    }
    k_sleep(K_MSEC(5));
    err=ap3216c_reg_read(dev,AP3216C_PS_HIGH_REG,&val[1]);
    if(err){
        return -EIO;
    }
    return 0;
}

static int ap3216c_sample_fetch(const struct device *dev, enum sensor_channel chan)
{
    const struct ap3216c_driver_config *cfg = dev->config;
	struct ap3216c_driver_data *drv_data = dev->data;
	uint8_t value[2];

	__ASSERT_NO_MSG(chan == SENSOR_CHAN_ALL ||
			chan == SENSOR_CHAN_LIGHT || chan == SENSOR_CHAN_PROX||chan == SENSOR_CHAN_DISTANCE);
    memset(value,0,2);
	if (ap3216c_asl_reg_read(dev, value) < 0) {
		return -EIO;
	}
    drv_data->asl=((uint16_t)value[1]<<8)|value[0]; 
    k_sleep(K_MSEC(cfg->intval));

	memset(value,0,2);
    if (ap3216c_ir_reg_read(dev, value) < 0) {
		return -EIO;
	}
    if(value[0]&0X80)
        drv_data->ir=0;						
	else 
        drv_data->ir=((uint16_t)value[1]<<2)|(value[0]&0X03);
    
	k_sleep(K_MSEC(cfg->intval));

    memset(value,0,2);
    if (ap3216c_ps_reg_read(dev, value) < 0) {
		return -EIO;
	}
    if(value[0]&0x40)
        drv_data->ps=0;    					
	else 
        drv_data->ps=((uint16_t)(value[1]&0X3F)<<4)|(value[0]&0X0F); 
	
	return 0;
}

static int ap3216c_attr_set(const struct device *dev,
			   enum sensor_channel chan,
			   enum sensor_attribute attr,
			   const struct sensor_value *val)
{
    return -ENOTSUP;
}

static int ap3216c_channel_get(const struct device *dev,
		enum sensor_channel chan,
		struct sensor_value *val)
{
	struct ap3216c_driver_data *drv_data = dev->data;
    //asl=count*0.35=xx/lux
    //ir/ps=count/2 m
	switch (chan)
    {
    //asl
    case SENSOR_CHAN_LIGHT:
        //printk("drv_data->asl:%d\n",drv_data->asl);
        val->val1=(uint16_t)(drv_data->asl*0.35);
        val->val2=0;
        break;
    //ps
    case SENSOR_CHAN_PROX:
        //printk("drv_data->ps:%d\n",drv_data->ps);
        val->val1=(uint16_t)(drv_data->ps/2);
        val->val2=0;
        break;
    //ir
    case SENSOR_CHAN_DISTANCE:
        //printk("drv_data->ir:%d\n",drv_data->ir);
        val->val1=(uint16_t)(drv_data->ir/2);
        val->val2=0;
        break;
    default:
        return -ENOTSUP;
    }
    
	return 0;
}

static const struct sensor_driver_api ap3216c_driver_api = {
	.attr_set = ap3216c_attr_set,
	.sample_fetch = ap3216c_sample_fetch,
	.channel_get = ap3216c_channel_get,
};


//pcf8574的初始化
static int ap3216c_init(const struct device *device)
{
    uint8_t tmp=0;
	const struct ap3216c_driver_config *cfg = device->config;
	struct ap3216c_driver_data *data = device->data;
	if((data!=NULL)&&(cfg!=NULL)){
		//获取iic的控制器驱动
		data->bus_device = device_get_binding(cfg->name);
		if(data->bus_device==NULL){
			return -1;
		}
        if(ap3216c_reg_write(device,0x00,0x03)<0){
            return -1;
        }
        if(ap3216c_reg_read(device,0x00,&tmp)<0){
            return -1;
        }
        if(tmp!=0x03){
            return -1;
        }
	}else{
		return -1;
	}
	return 0;
}

#if DT_NODE_EXISTS(DT_NODELABEL(ap3216c))

static struct ap3216c_driver_data ap3216c_driver = {
    //中断gpio的状态
	.gpio_device=NULL,
    //gpio的label名字
    .gpio_name=NULL,
    //gpio的port
    .pin=0,
    //驱动的名字
	.name=DT_LABEL(DT_NODELABEL(ap3216c)),
    //bus的设备
	.bus_device=NULL,
    //IR数据
    .ir=0,
    //asl数据
    .asl=0,
    //ps 数据
    .ps=0,
};

static const struct ap3216c_driver_config ap3216c_config = {
	//IIC的地址
    .address=DT_REG_ADDR(DT_NODELABEL(ap3216c)),
    //bus的label名字
    .name=DT_LABEL(DT_BUS(DT_NODELABEL(ap3216c))),
    //IIC操作超时时间
    .timeout=DT_PROP(DT_NODELABEL(ap3216c), timeout),
    //间隔时间
    .intval=DT_PROP(DT_NODELABEL(ap3216c), interval),
};

DEVICE_AND_API_INIT(ap3216c, DT_LABEL(DT_NODELABEL(ap3216c)), ap3216c_init, &ap3216c_driver,
		    &ap3216c_config, POST_KERNEL, CONFIG_APPLICATION_INIT_PRIORITY,
		    &ap3216c_driver_api);
#endif
