#ifndef _APP_STM32F7_AP3216C_H_
#define _APP_STM32F7_AP3216C_H_

#include<zephyr.h>
#include <device.h>

#define AP3216C_SYS_SET_REG         0x00        //System Configuration Control of basic functions
#define AP3216C_INTER_STATUS_REG    0x01        //Interrupt Status ALS and PS interrupt status output
#define AP3216C_INTER_CLEAR_REG     0x02        //INT Clear Manner Auto/semi clear INT pin selector
#define AP3216C_IR_LOW_REG          0x0A        //IR Data Low Lower byte for IR ADC channel output
#define AP3216C_IR_HIGH_REG         0x0B        //IR Data High Higher byte for IR ADC channel output
#define AP3216C_ALS_LOW_REG         0x0C        //ALS Data Low Lower byte for ALS ADC channel output
#define AP3216C_ALS_HIGH_REG        0x0D        //ALS Data High Higher byte for ALS ADC channel output
#define AP3216C_PS_LOW_REG          0x0E        //PS Data Low Lower byte for PS ADC channel output
#define AP3216C_PS_HIGH_REG         0x0F        //PS Data High Higher byte for PS ADC channel output
/**
 * ap3216c的config结构
 */ 
struct ap3216c_driver_config {
    //IIC的地址
    uint16_t address;
    //bus的label名字
    uint8_t *name;
    //IIC操作超时时间
    uint16_t timeout;
    //间隔时间
    uint16_t intval;
};

/**
 * ap3216c的data结构
 */ 
struct ap3216c_driver_data {
    //中断gpio的状态
	const struct device *gpio_device;
    //gpio的label名字
    uint8_t *gpio_name;
    //gpio的port
    uint32_t pin;
    //驱动的名字
	uint8_t *name;
    //bus的设备
	const struct device *bus_device;
    //IR数据
    uint16_t ir;
    //asl数据
    uint16_t asl;
    //ps 数据
    uint16_t ps;
};

#endif