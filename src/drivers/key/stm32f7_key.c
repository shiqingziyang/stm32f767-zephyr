#include "stm32f7_key.h"
#include <kernel.h>
#include <device.h>
#include <drivers/gpio.h>


int key_stm32_interupte(const struct device *device,gpio_callback_handler_t callback)
{
    int ret;
    uint32_t flag;
    struct key_driver_data *data = device->data;
	const struct key_driver_config *cfg = device->config;
    if(data->gpio_device!=NULL){
        if(cfg->state == GPIO_ACTIVE_LOW){
            flag=GPIO_INT_EDGE_RISING;
            ret = gpio_pin_configure(data->gpio_device, cfg->pin, GPIO_INPUT | GPIO_INT_DEBOUNCE|GPIO_PULL_DOWN|GPIO_OUTPUT_LOW);
            if(ret){
                return -1;
            }
        }else{
            flag=GPIO_INT_EDGE_FALLING;
            ret = gpio_pin_configure(data->gpio_device, cfg->pin, GPIO_INPUT | GPIO_INT_DEBOUNCE|GPIO_PULL_UP|GPIO_OUTPUT_HIGH);
            if(ret){
                return -1;
            }
        }
		gpio_init_callback(&data->gpio_cb, callback, BIT(cfg->pin));
	    gpio_add_callback(data->gpio_device, &data->gpio_cb);
	    return gpio_pin_interrupt_configure(data->gpio_device, cfg->pin, flag);
	}else{
		return -1;
	}
	return 0;
}
// 1:按下 0：没有按下 -1：失败
int is_key_stm32_down(const struct device *device)
{
    int ret;
	uint32_t flag;
    const struct key_driver_data *data = device->data;
	const struct key_driver_config *cfg = device->config;
    if(data->gpio_device!=NULL){
		if(cfg->state==GPIO_ACTIVE_LOW){
			flag = GPIO_PULL_DOWN|GPIO_OUTPUT_LOW;
		}else
		{
			flag = GPIO_PULL_UP|GPIO_OUTPUT_HIGH;
		}
		
        ret = gpio_pin_configure(data->gpio_device, cfg->pin, GPIO_INPUT|GPIO_INT_DEBOUNCE|flag);
        if(ret){
            return -1;
        }
        if(gpio_pin_get_raw(data->gpio_device, cfg->pin)==cfg->state){
            return 1;
        }
    }else
    {
        return -1;
    }
	return 0;
}
static const struct key_driver_api key_stm32_driver = {
	.key_interupte=key_stm32_interupte,
    .is_key_down=is_key_stm32_down,
};
static int key_stm32_init(const struct device *device)
{
	const struct key_driver_config *cfg = device->config;
	struct key_driver_data *data = device->data;
	if(data!=NULL){
		data->gpio_device = device_get_binding(cfg->basename);
		if(data->gpio_device==NULL){
			return -1;
		}
	}else{
		return -1;
	}
	return 0;
}

#define KEY_DEVICE_INIT(__name, __suffix, __base_name, __port,__state) 				\
	static const struct key_driver_config key_stm32_cfg_## __suffix = {   	\
		.basename = __base_name,												\
        .pin = __port,														\
        .state = __state,                                                       \
    };	                							       									\
	static struct key_driver_data key_stm32_data_## __suffix = {						\
    	.name = __name,														\
		.gpio_device = NULL,												\
    };	       																\
	DEVICE_AND_API_INIT(key_stm32_## __suffix,			       \
			    __name,					       \
			    key_stm32_init,				       \
			    &key_stm32_data_## __suffix,		       \
			    &key_stm32_cfg_## __suffix,		       \
			    POST_KERNEL,				       \
			    CONFIG_KERNEL_INIT_PRIORITY_DEVICE,	       \
			    &key_stm32_driver)

#define KEY_DEVICE_INIT_STM32(__suffix)						\
	KEY_DEVICE_INIT(DT_LABEL(DT_NODELABEL(button##__suffix)),	            \
			 __suffix,					\
			 DT_LABEL(DT_PHANDLE(DT_NODELABEL(button##__suffix),gpios)),		\
			 DT_PHA_BY_IDX(DT_NODELABEL(button##__suffix), gpios, 0, pin),      \
             DT_PHA_BY_IDX(DT_NODELABEL(button##__suffix), gpios, 0, flags))

#if DT_NODE_EXISTS(DT_NODELABEL(button0))
	KEY_DEVICE_INIT_STM32(0);
#endif
#if DT_NODE_EXISTS(DT_NODELABEL(button1))
	KEY_DEVICE_INIT_STM32(1);
#endif
#if DT_NODE_EXISTS(DT_NODELABEL(button2))
	KEY_DEVICE_INIT_STM32(2);
#endif
#if DT_NODE_EXISTS(DT_NODELABEL(button3))
	KEY_DEVICE_INIT_STM32(3);
#endif