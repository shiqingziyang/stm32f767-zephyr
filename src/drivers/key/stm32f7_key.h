#ifndef _APP_STM32F7_KEY_H_
#define _APP_STM32F7_KEY_H_
#include<zephyr.h>
#include <device.h>
#include <drivers/gpio.h>
//当为wkup时高电平有效
#define WKUP_ACTIVE 1
//当为其他按键时低电平有效
#define KEY_ACTIVE 0
/**
 * key的api结构
 */ 
struct key_driver_api {
    //led注册案件案件按下事件
	int (*key_interupte)(const struct device *device,gpio_callback_handler_t callback);
    //判断案件是否按下
	int (*is_key_down)(const struct device *port);
};

/**
 * key的config结构
 */ 
struct key_driver_config {
	/* port name */
	uint8_t *basename;
	/* pin */
	int pin;
    /*state*/
    int state;
};

/**
 * key的data结构
 */ 
struct key_driver_data {
	uint8_t *name;
    const struct device *gpio_device;
    struct gpio_callback gpio_cb;
};

#endif