#include "stm32f7_led.h"
#include <kernel.h>
#include <device.h>
#include <drivers/gpio.h>

int led_stm32_on(const struct device *device)
{
	int res;
	const struct led_driver_data *data = device->data;
	const struct led_driver_config *cfg = device->config;
	if(data->gpio_device!=NULL){
		res=gpio_pin_set_raw(data->gpio_device,cfg->pin,cfg->state);
		return res;
	}else{
		return -1;
	}
}

int led_stm32_off(const struct device *device)
{
	int res;
    const struct led_driver_data *data = device->data;
	const struct led_driver_config *cfg = device->config;
	if(data->gpio_device!=NULL){
		res=gpio_pin_set_raw(data->gpio_device,cfg->pin,!cfg->state);
		return res;
	}else{
		return -1;
	}
	
}
static const struct led_driver_api led_stm32_driver = {
	.led_on=led_stm32_on,
    .led_off=led_stm32_off,
};
static int led_stm32_init(const struct device *device)
{
	const struct led_driver_config *cfg = device->config;
	struct led_driver_data *data = device->data;
	if(data!=NULL){
		data->gpio_device = device_get_binding(cfg->basename);
		if(data->gpio_device){
			return gpio_pin_configure(data->gpio_device, cfg->pin, GPIO_OUTPUT_HIGH);
		}else{
			return -1;
		}
	}else{
		return -1;
	}
	return 0;
}

#define LED_DEVICE_INIT(__name, __suffix, __base_name, __port) 				\
	static const struct led_driver_config led_stm32_cfg_## __suffix = {   	\
		.basename = __base_name,												\
        .pin = __port,														\
        .state = LED_ACTIVE,												\
	};								       									\
	static struct led_driver_data led_stm32_data_## __suffix = {						\
    	.name = __name,														\
		.gpio_device = NULL,												\
	};	       																\
	DEVICE_AND_API_INIT(led_stm32_## __suffix,			       \
			    __name,					       \
			    led_stm32_init,				       \
			    &led_stm32_data_## __suffix,		       \
			    &led_stm32_cfg_## __suffix,		       \
			    POST_KERNEL,				       \
			    CONFIG_KERNEL_INIT_PRIORITY_DEVICE,	       \
			    &led_stm32_driver)

#define LED_DEVICE_INIT_STM32(__suffix)						\
	LED_DEVICE_INIT(DT_LABEL(DT_NODELABEL(led##__suffix)),	\
			 __suffix,					\
			 DT_LABEL(DT_PHANDLE(DT_NODELABEL(led##__suffix),gpios)),		\
			 DT_PHA_BY_IDX(DT_NODELABEL(led##__suffix), gpios, 0, pin))

#if DT_NODE_EXISTS(DT_NODELABEL(led0))
	LED_DEVICE_INIT_STM32(0);
#endif

#if DT_NODE_EXISTS(DT_NODELABEL(led1))
	LED_DEVICE_INIT_STM32(1);
#endif