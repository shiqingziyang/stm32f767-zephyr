#ifndef _APP_STM32F7_LED_H_
#define _APP_STM32F7_LED_H_
#include<zephyr.h>
#include <device.h>

//为低电平时代表亮
#define LED_ACTIVE 0
/**
 * led的api结构
 */ 
struct led_driver_api {
    //led亮
	int (*led_on)(const struct device *port);
    //led灭
	int (*led_off)(const struct device *port);
};

/**
 * led的config结构
 */ 
struct led_driver_config {
	/* port name */
	uint8_t *basename;
	/* pin */
	int pin;
	/* 点亮需要的电瓶状态 */
    bool state;
};

/**
 * led的data结构
 */ 
struct led_driver_data {
	uint8_t *name;
    const struct device *gpio_device;
};


#endif