
#ifndef TEST_SAMPLE_MODULE_CMD_H
#define TEST_SAMPLE_MODULE_CMD_H

#include <logging/log.h>

#ifdef __cplusplus
extern "C" {
#endif

#define MODULE_NAME test_sample_module

const char *module_name_get(void);
void module_output_func(void);

static inline void module_inline_output_func(void)
{
    //在.h文件中使用日志系统时，必须先实例
	LOG_MODULE_DECLARE(MODULE_NAME, CONFIG_SAMPLE_MODULE_CMD_LOG_LEVEL);
    
	LOG_INF("测试.h文件中日志的输出");
}

#ifdef __cplusplus
}
#endif

#endif /* SAMPLE_MODULE_H */
