/*
 * Copyright (c) 2018 Nordic Semiconductor ASA
 *
 * SPDX-License-Identifier: Apache-2.0
 */
#ifndef TEST_SAMPLE_INSTANCE_CMD_H
#define TEST_SAMPLE_INSTANCE_CMD_H

#include <kernel.h>
#include <logging/log_instance.h>
#include <logging/log.h>

#define SAMPLE_INSTANCE_NAME test_sample_instance

struct sample_instance {
	LOG_INSTANCE_PTR_DECLARE(log);
	uint32_t cnt;
};

//建立一个日志实例宏
#define SAMPLE_INSTANCE_DEFINE(_name)				   \
	LOG_INSTANCE_REGISTER(SAMPLE_INSTANCE_NAME, _name, LOG_LEVEL_INF); \
	struct sample_instance _name = {		   \
		LOG_INSTANCE_PTR_INIT(log, SAMPLE_INSTANCE_NAME, _name)	   \
	}

//日志消息的输出
void sample_instance_output(struct sample_instance *inst);

static inline void sample_instance_inline_output(struct sample_instance *inst)
{
	//设置日志级别
	LOG_LEVEL_SET(LOG_LEVEL_INF);

	LOG_INST_INF(inst->log, "Inline call.");
}

#endif /*SAMPLE_INSTANCE_H*/
