/*
 * Copyright (c) 2018 Nordic Semiconductor ASA
 *
 * SPDX-License-Identifier: Apache-2.0
 */
#ifndef TEST_EXT_LOG_SYSTEM_CMD_H
#define TEST_EXT_LOG_SYSTEM_CMD_H

#ifdef __cplusplus
extern "C" {
#endif

/** Log message priority levels */
enum ext_log_level {
	EXT_LOG_CRITICAL,
	EXT_LOG_ERROR,
	EXT_LOG_WARNING,
	EXT_LOG_NOTICE,
	EXT_LOG_INFO,
	EXT_LOG_DEBUG
};

/** log处理函数原型 */
typedef void (*ext_log_handler)(enum ext_log_level level,
				const char *format, ...);

/** @brief 设置log的处理器
 *
 * @param handler 处理器.
 */
void ext_log_handler_set(ext_log_handler handler);

/** @brief 测试输出消息的. */
void ext_log_system_out(void);

/** @brief 定义一个通用的log API. */
#define ext_log(level, ...) log_handler(level, __VA_ARGS__)

#ifdef __cplusplus
}
#endif

#endif /* EXT_LOG_SYSTEM_H */
