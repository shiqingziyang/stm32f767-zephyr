#include <zephyr.h>
#include <logging/log.h>
#include "sample_module_cmd.h"

//注册日志模块，日志默认级别在Kconfig中设置
LOG_MODULE_REGISTER(MODULE_NAME, CONFIG_SAMPLE_MODULE_CMD_LOG_LEVEL);

//获取日志模块名字的字符串形式
const char *module_name_get(void)
{
	return STRINGIFY(MODULE_NAME);
}

//日志模块的输出
void module_output_func(void)
{
	LOG_INF("测试.c文件中日志的输出 %d", 11);
}