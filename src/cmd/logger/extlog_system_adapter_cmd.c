#include "extlog_system_adapter_cmd.h"
#include "extlog_system_cmd.h"

#define LOG_MODULE_NAME test_ext_log_system
#include <logging/log.h>
//注册logo模块,采用系统默认log级别
LOG_MODULE_REGISTER(LOG_MODULE_NAME);

/** @brief 传输的log级别. */
static const uint8_t log_level_lut[] = {
	LOG_LEVEL_ERR,  /* EXT_LOG_CRITICAL */
	LOG_LEVEL_ERR,  /* EXT_LOG_ERROR */
	LOG_LEVEL_WRN,  /* EXT_LOG_WARNING */
	LOG_LEVEL_INF,  /* EXT_LOG_NOTICE */
	LOG_LEVEL_INF,  /* EXT_LOG_INFO */
	LOG_LEVEL_DBG   /* EXT_LOG_DEBUG */
};
/**
 * log处理函数
 */ 
static void log_handler(enum ext_log_level level, const char *fmt, ...)
{
	struct log_msg_ids src_level = {
		.level = log_level_lut[level],
		.domain_id = CONFIG_LOG_DOMAIN_ID,
		.source_id = LOG_CURRENT_MODULE_ID()
	};

	va_list ap;
	//可以做一些其他的事情
	//.......
	va_start(ap, fmt);
	//将log信息写入log系统中
	log_generic(src_level, fmt, ap, LOG_STRDUP_CHECK_EXEC);
	va_end(ap);
}

/**
 * 将log处理函数安装进入
 */ 
void ext_log_system_log_adapt(void)
{
	ext_log_handler_set(log_handler);
}
