#include <zephyr.h>
#include <shell/shell.h>
#include <version.h>
#include <logging/log_ctrl.h>
#include <logging/log.h>
#include "sample_module_cmd.h"
#include "sample_instance_cmd.h"
#include "extlog_system_adapter_cmd.h"
#include "extlog_system_cmd.h"

//定义实例名称1
#define INST1_NAME STRINGIFY(SAMPLE_INSTANCE_NAME.inst1)
SAMPLE_INSTANCE_DEFINE(inst1);

//定义实例名称2
#define INST2_NAME STRINGIFY(SAMPLE_INSTANCE_NAME.inst2)
SAMPLE_INSTANCE_DEFINE(inst2);

/**
 * @brief 通过模块名字查找源id.
 *
 * @param name 模块名字
 *
 * @return 源id
 */
static int log_source_id_get(const char *name)
{

	for (int i = 0; i < log_src_cnt_get(CONFIG_LOG_DOMAIN_ID); i++) {
		if (strcmp(log_source_name_get(CONFIG_LOG_DOMAIN_ID, i), name)
		    == 0) {
			return i;
		}
	}
	return -1;
}

/**
 * 测试模块的日志
 */ 
static int cmd_test_sample_module(const struct shell *shell, size_t argc, char **argv)
{
    //输出日志
	module_output_func();
	module_inline_output_func();
    //判断是否打开了日志系统的运行时配置
    if (IS_ENABLED(CONFIG_LOG_RUNTIME_FILTERING)) {
		shell_info(shell,"关闭消息的输出在%s模块\n",
					module_name_get());
        //配置过滤器去关闭在模块上的输出
		log_filter_set(NULL, 0,
			       log_source_id_get(module_name_get()),
			       LOG_LEVEL_NONE);

		module_output_func();

		shell_info(shell,"输出消息已经被调用\n");

	} else {
		shell_info(shell,"%s option disabled.\n",
		       STRINGIFY(CONFIG_LOG_RUNTIME_FILTERING));
	}
	return 0;
}

/**
 * 测试实例的日志
 */ 
static int cmd_test_sample_instance(const struct shell *shell, size_t argc, char **argv)
{
	sample_instance_inline_output(&inst1);
	sample_instance_output(&inst1);
	sample_instance_inline_output(&inst2);
	sample_instance_output(&inst2);

	if (IS_ENABLED(CONFIG_LOG_RUNTIME_FILTERING)) {
		shell_info(shell,"Changing filter to warning on %s instance.\n",
								INST1_NAME);

		log_filter_set(NULL, 0,
			       log_source_id_get(INST1_NAME), LOG_LEVEL_WRN);

		sample_instance_inline_output(&inst1);
		sample_instance_output(&inst1);
		sample_instance_inline_output(&inst2);
		sample_instance_output(&inst2);

		shell_info(shell,"Disabling logging on both instances.\n");

		log_filter_set(NULL, 0,
			       log_source_id_get(INST1_NAME),
			       LOG_LEVEL_NONE);

		log_filter_set(NULL, 0,
			       log_source_id_get(INST2_NAME),
			       LOG_LEVEL_NONE);

		sample_instance_inline_output(&inst1);
		sample_instance_output(&inst1);
		sample_instance_inline_output(&inst2);
		sample_instance_output(&inst2);

		shell_info(shell,"Function call on both instances with logging disabled.\n");
	}
	return 0;
}

/**
 * 测试扩展的日志
 */ 
static int cmd_test_extlog(const struct shell *shell, size_t argc, char **argv)
{
	shell_info(shell,"Logs from external logging system showcase.\n");

	ext_log_system_log_adapt();

	ext_log_system_out();
	return 0;
}

SHELL_STATIC_SUBCMD_SET_CREATE(sub_test_log,
	SHELL_CMD(samplemodule, NULL, "sample module cmd.", cmd_test_sample_module),
	SHELL_CMD(sampleinstance, NULL, "sample instance cmd.", cmd_test_sample_instance),
	SHELL_CMD(extlog, NULL, "test extlog cmd.", cmd_test_extlog),
	SHELL_SUBCMD_SET_END /* Array terminated. */
);
SHELL_CMD_REGISTER(testlog, &sub_test_log, "Test logger", NULL);
