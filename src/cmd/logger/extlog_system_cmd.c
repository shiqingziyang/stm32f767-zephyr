#include <app_memory/app_memdomain.h>
#include "extlog_system_cmd.h"

static ext_log_handler log_handler;

/**
 * 设置log的处理器
 */ 
void ext_log_handler_set(ext_log_handler handler)
{
	log_handler = handler;
}

/**
 * 扩展的log的信息输出
 */ 
void ext_log_system_out(void)
{
	ext_log(EXT_LOG_CRITICAL, "critical level log");

	ext_log(EXT_LOG_ERROR, "error level log, 1 arguments: %d", 1);

	ext_log(EXT_LOG_WARNING, "warning level log, 2 arguments: %d %d", 1, 2);

	ext_log(EXT_LOG_NOTICE, "notice level log, 3 arguments: %d, %s, 0x%08x",
							100, "string", 0x255);

	ext_log(EXT_LOG_INFO, "info level log, 4 arguments : %d %d %d %d",
								1, 2, 3, 4);

	ext_log(EXT_LOG_DEBUG, "debug level log, 5 arguments: %d %d %d %d %d",
								1, 2, 3, 4, 5);
}
