#include <zephyr.h>
#include <shell/shell.h>
#include <stdlib.h>
#include <drivers/watchdog.h>
const struct device *wdt_dev=NULL;
static struct wdt_timeout_cfg m_cfg_wdt;

static int cmd_test_iwdg_start(const struct shell *shell, size_t argc, char **argv)
{
	int err;
	if(wdt_dev){
		shell_error(shell,"wdt is run");
		return -1;
	}
	wdt_dev = device_get_binding("IWDG");
	if(wdt_dev==NULL){
		shell_error(shell,"dev is Null");
		return -1;
	}
	m_cfg_wdt.callback = NULL;
	m_cfg_wdt.flags = WDT_FLAG_RESET_SOC;
	m_cfg_wdt.window.max = 10000U; //10s
	err = wdt_install_timeout(wdt_dev, &m_cfg_wdt);
	if (err < 0) {
		shell_error(shell,"Watchdog install error\n");
	}
	err = wdt_setup(wdt_dev, WDT_OPT_PAUSE_HALTED_BY_DBG);
	if(err){
		shell_error(shell,"wdt is start error:%d",err);
	}
	return err;
}

static int cmd_test_iwdg_feed(const struct shell *shell, size_t argc, char **argv)
{
	int err;
	if(wdt_dev==NULL){
		shell_error(shell,"wdt is no run");
		return -1;
	}
	err = wdt_feed(wdt_dev,0);
	if(err){
		shell_error(shell,"wdt is feed error:%d",err);
	}
	return err;
}

SHELL_STATIC_SUBCMD_SET_CREATE(iwdg_test,
	SHELL_CMD(start, NULL, "iwdg start", cmd_test_iwdg_start),
	SHELL_CMD(feed, NULL, "iwdg feed", cmd_test_iwdg_feed),
	SHELL_SUBCMD_SET_END /* Array terminated. */
);
SHELL_CMD_REGISTER(iwdg, &iwdg_test, "iwdg driver", NULL);
