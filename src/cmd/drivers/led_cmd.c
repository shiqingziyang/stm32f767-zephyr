#include <zephyr.h>
#include <shell/shell.h>
#include <stdlib.h>
#include <device.h>
#include "stm32f7_led.h"

static int cmd_test_on_led(const struct shell *shell, size_t argc, char **argv)
{
    int res=0;
	const struct device *dev=NULL;
    const struct led_driver_api *api=NULL;
    if(argc!=2){
        shell_error(shell,"agrc is led[0|1]");
        return -1;
    }
    if((strncmp(argv[1],"led0",sizeof("led0"))!=0)&&(strncmp(argv[1],"led1",sizeof("led1"))!=0)){
        shell_error(shell,"agrc is led[0|1]");
        return -1;
    }
	dev = device_get_binding(argv[1]);
    if(dev!=NULL){
        api = (const struct led_driver_api *)dev->api;
        if(api!=NULL){
            res=api->led_on(dev);
            if(!res){
                shell_info(shell, "led on ok");
            }else{
                shell_error(shell, "led on fail");
                return -1;
            }
        }else{
            shell_error(shell, "api is null");
            return -1;
        }
    }else{
        shell_error(shell, "dev is null");
        return -1;
    }
	return 0;
}

static int cmd_test_off_led(const struct shell *shell, size_t argc, char **argv)
{
    int res;
	const struct device *dev=NULL;
    const struct led_driver_api *api=NULL;
    if(argc!=2){
        shell_error(shell,"agrc is led[0|1]");
        return -1;
    }
    if((strncmp(argv[1],"led0",sizeof("led0"))!=0)&&(strncmp(argv[1],"led1",sizeof("led1"))!=0)){
        shell_error(shell,"agrc is led[0|1]");
        return -1;
    }
	dev = device_get_binding(argv[1]);
    if(dev!=NULL){
        api = (const struct led_driver_api *)dev->api;
        if(api!=NULL){
            res=api->led_off(dev);
            if(!res){
                shell_info(shell, "led off ok");
            }else{
                shell_error(shell, "led off fail");
                return -1;
            }
        }else{
            shell_error(shell, "api is null");
            return -1;
        }
    }else{
        shell_error(shell, "dev is null");
        return -1;  
    }
	return 0;
}

SHELL_STATIC_SUBCMD_SET_CREATE(led_test,
	SHELL_CMD(on, NULL, "open led[0|1]", cmd_test_on_led),
	SHELL_CMD(off, NULL, "close led[0|1]", cmd_test_off_led),
	SHELL_SUBCMD_SET_END /* Array terminated. */
);
SHELL_CMD_REGISTER(led, &led_test, "led drive", NULL);