#include <zephyr.h>
#include <shell/shell.h>
#include <stdlib.h>
#include <drivers/uart.h>
#include <logging/log.h>
#define LOG_MODULE_NAME drivers_cmd_uart
LOG_MODULE_REGISTER(LOG_MODULE_NAME, LOG_LEVEL_DBG);

#define MAX_REVICE 100
struct reice_data
{
    uint16_t len;
    /* data */
    uint8_t data[MAX_REVICE];
};


static k_tid_t uart_test_thread_id=NULL;
#define UART_TEST_THREAD_SIZE 100
#define UART_TEST_PRIORITY 10
K_THREAD_STACK_DEFINE(uart_test_thread_stack_area, UART_TEST_THREAD_SIZE);
static struct k_thread uart_test_thread_data;
static volatile bool data_received=false;
static const struct device *uart_dev=NULL;
static struct reice_data rdata;
static void uart_revice_timeout(struct k_timer *dummy);
K_TIMER_DEFINE(revice_timer, uart_revice_timeout, NULL);
static void uart_revice_timeout(struct k_timer *dummy)
{
    //但没有数据数据的在过来的时候直接置为标志
    data_received = true;
    k_timer_stop(&revice_timer);
}
static void uart_fifo_callback(const struct device *dev, void *user_data)
{
	uint8_t recvData;

	ARG_UNUSED(user_data);

	/* Verify uart_irq_update() */
	if (!uart_irq_update(dev)) {
		LOG_INF("retval should always be 1\n");
		return;
	}

	/* Verify uart_irq_rx_ready() */
	if (uart_irq_rx_ready(dev)) {
		/* Verify uart_fifo_read() */
		uart_fifo_read(dev, &recvData, 1);
		//LOG_INF("%c", recvData);
        rdata.data[rdata.len++]=recvData;
        k_timer_start(&revice_timer, K_MSEC(200), K_MSEC(200));
	}
}

static void uart_test_thread(void *uart_dev,void *p2,void *p3)
{
    int i=0;
    ARG_UNUSED(p2);
    ARG_UNUSED(p3);
    const struct device* dev = (const struct device *)uart_dev;
    memset(&rdata,0,sizeof(rdata));
    while(1){
       /* Verify uart_irq_callback_set() */
        uart_irq_callback_set(dev, uart_fifo_callback);

        /* Enable Tx/Rx interrupt before using fifo */
        /* Verify uart_irq_rx_enable() */
        uart_irq_rx_enable(dev);
        
        if(data_received == true) {
            LOG_INF("revice-len:[%d],revice-data:[%s]",rdata.len,rdata.data);
            for (i = 0; i < rdata.len; i++) {
                uart_poll_out(dev, rdata.data[i]);
            }
            memset(&rdata,0,sizeof(rdata));
            data_received=false;
        }
        k_sleep(K_MSEC(100));
    }
}

static int cmd_test_uart_start(const struct shell *shell, size_t argc, char **argv)
{
    if(argc!=2){
        shell_error(shell,"agrc is 'UART driver name'");
        return -1;
    }
    uart_dev = device_get_binding(argv[1]);
    if(uart_dev==NULL){
        shell_error(shell,"agrc is 'UART driver name'");
        return -1;
    }
    if(uart_test_thread_id!=NULL){
        shell_error(shell,"UART Thread is run");
        return -1;
    }
    //创建一个uart线程
    uart_test_thread_id = k_thread_create(&uart_test_thread_data, uart_test_thread_stack_area,
                                         K_THREAD_STACK_SIZEOF(uart_test_thread_stack_area),
                                         uart_test_thread,
                                         (void *)uart_dev, NULL, NULL,
                                         UART_TEST_PRIORITY, 0, K_NO_WAIT);
    return 0;
}

static int cmd_test_uart_stop(const struct shell *shell, size_t argc, char **argv)
{
    ARG_UNUSED(shell);
    ARG_UNUSED(argc);
    ARG_UNUSED(argv);
    if(uart_dev==NULL){
        shell_error(shell,"uart thread no run");
        return -1;
    }
    uart_irq_tx_disable(uart_dev);
    k_thread_abort(uart_test_thread_id);
    uart_test_thread_id=NULL;
    uart_dev=NULL;
    return 0;
}

SHELL_STATIC_SUBCMD_SET_CREATE(uart_test,
	SHELL_CMD(start, NULL, "uart thread start", cmd_test_uart_start),
    SHELL_CMD(stop, NULL, "uart thread stop", cmd_test_uart_stop),
	SHELL_SUBCMD_SET_END /* Array terminated. */
);
SHELL_CMD_REGISTER(uart, &uart_test, "uart drive", NULL);