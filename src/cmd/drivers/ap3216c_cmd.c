#include <zephyr.h>
#include <shell/shell.h>
#include <stdlib.h>
#include <drivers/sensor.h>


static int cmd_test_get_ap3216c_data(const struct shell *shell, size_t argc, char **argv)
{
	const struct device *dev = device_get_binding("AP3216C");
	struct sensor_value asl, ps, ir;
	if (dev == NULL) {
		shell_error(shell,"No device AP3216C found; did initialization fail\n");
		return -1;
	}
	
	sensor_sample_fetch(dev);
	sensor_channel_get(dev, SENSOR_CHAN_LIGHT, &asl);
	sensor_channel_get(dev, SENSOR_CHAN_PROX, &ps);
	sensor_channel_get(dev, SENSOR_CHAN_DISTANCE, &ir);

	shell_info(shell,"asl: %d; ps: %d; ir: %d\n",
			asl.val1,  ps.val1, ir.val1);
	return 0;
}


SHELL_CMD_ARG_REGISTER(ap3216c, NULL, "ap3216c driver", cmd_test_get_ap3216c_data, 1, 0);