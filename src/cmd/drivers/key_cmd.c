#include <zephyr.h>
#include <shell/shell.h>
#include <stdlib.h>
#include <device.h>
#include <drivers/gpio.h>
#include "stm32f7_key.h"

static void callback(const struct device *device, struct gpio_callback *cb,
			  gpio_port_pins_t pins)
{
    printk("gpio_pin:%d\n",pins);
}
static int cmd_test_key_interupter(const struct shell *shell, size_t argc, char **argv)
{
    int res=0;
	const struct device *dev=NULL;
    const struct key_driver_api *api=NULL;
    if(argc!=2){
        shell_error(shell,"agrc is wkup|key[0|1|2]");
        return -1;
    }
    if((strncmp(argv[1],"wkup",sizeof("wkup"))!=0)&&
        (strncmp(argv[1],"key0",sizeof("key0"))!=0)&&
        (strncmp(argv[1],"key1",sizeof("key1"))!=0)&&
        (strncmp(argv[1],"key2",sizeof("key2"))!=0)){
        shell_error(shell,"agrc is wkup|key[0|1|2]");
        return -1;
    }
	dev = device_get_binding(argv[1]);
    if(dev!=NULL){
        api = (const struct key_driver_api *)dev->api;
        if(api!=NULL){
            res=api->key_interupte(dev,callback);
            if(res==0){
                shell_info(shell, "key interup ok");
            }else{
                shell_error(shell, "key interup fail");
                return -1;
            }
        }else{
            shell_error(shell, "api is null");
            return -1;
        }
    }else{
        shell_error(shell, "dev is null");
        return -1;
    }
	return 0;
}

static int cmd_test_key_is_down(const struct shell *shell, size_t argc, char **argv)
{
    int res=0;
	const struct device *dev=NULL;
    const struct key_driver_api *api=NULL;
    if(argc!=2){
        shell_error(shell,"agrc is wkup|key[0|1|2]");
        return -1;
    }
    if((strncmp(argv[1],"wkup",sizeof("wkup"))!=0)&&
        (strncmp(argv[1],"key0",sizeof("key0"))!=0)&&
        (strncmp(argv[1],"key1",sizeof("key1"))!=0)&&
        (strncmp(argv[1],"key2",sizeof("key2"))!=0)){
        shell_error(shell,"agrc is wkup|key[0|1|2]");
        return -1;
    }
	dev = device_get_binding(argv[1]);
    if(dev!=NULL){
        api = (const struct key_driver_api *)dev->api;
        if(api!=NULL){
            res=api->is_key_down(dev);
            if(res>0){
                shell_info(shell, "key is down ok");
            }else if(res==0){
                shell_info(shell, "key is up ok");
            }else{
                shell_error(shell, "key is down fail");
                return -1;
            }
        }else{
            shell_error(shell, "api is null");
            return -1;
        }
    }else{
        shell_error(shell, "dev is null");
        return -1;
    }
	return 0;
}

SHELL_STATIC_SUBCMD_SET_CREATE(key_test,
	SHELL_CMD(interupter, NULL, "key interupter", cmd_test_key_interupter),
    SHELL_CMD(is_down, NULL, "key is down", cmd_test_key_is_down),
	SHELL_SUBCMD_SET_END /* Array terminated. */
);
SHELL_CMD_REGISTER(key, &key_test, "key drive", NULL);