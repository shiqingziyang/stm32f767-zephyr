#include <zephyr.h>
#include <shell/shell.h>
#include <stdlib.h>
#include "stm32f7_pcf8574.h"

const struct device *pcf8574_dev=NULL;

static int cmd_test_write_pcf8574(const struct shell *shell, size_t argc, char **argv)
{
	int err;
	int bit;
	int data=-1;
	if(argc!=3){
        shell_error(shell,"agrc is bit state");
        return -1;
    }
	bit=atoi(argv[1]);
	if((bit<0)||(bit>8)){
		shell_error(shell,"agrc is bit state");
        return -1;
	}
    if((strncmp(argv[2],"1",sizeof("1"))!=0)&&(strncmp(argv[2],"0",sizeof("0"))!=0)){
        shell_error(shell,"agrc is bit state");
        return -1;
    }
	data=atoi(argv[2]);
	pcf8574_dev = device_get_binding("PCF8574");
	if(pcf8574_dev==NULL){
		shell_error(shell,"dev is Null");
		return -1;
	}
	const struct pcf8574_driver_api *api=(const struct pcf8574_driver_api *)pcf8574_dev->api;
	if(api==NULL){
        shell_error(shell,"api get fail");
		return -1;
    }
	err=api->writebit(pcf8574_dev,bit,data);
	if(err){
		shell_error(shell,"pcf8574 write fail");
		return -1;
	}
	return 0;
}

static int cmd_test_read_pcf8574(const struct shell *shell, size_t argc, char **argv)
{
	int err;
	pcf8574_dev = device_get_binding("PCF8574");
	if(pcf8574_dev==NULL){
		shell_error(shell,"dev is Null");
		return -1;
	}
	const struct pcf8574_driver_api *api=(const struct pcf8574_driver_api *)pcf8574_dev->api;
	if(api==NULL){
        shell_error(shell,"api get fail");
		return -1;
    }
	err=api->readbit(pcf8574_dev,PCF8574_EXIO);
	if(err<0){
		shell_error(shell,"pcf8574 write fail");
		return -1;
	}
	shell_info(shell,"EXIO state is:[%d]\n",err);
	return 0;
}


SHELL_STATIC_SUBCMD_SET_CREATE(pcf8574_test,
	SHELL_CMD(write, NULL, "pcf8574 write", cmd_test_write_pcf8574),
	SHELL_CMD(read, NULL, "pcf8574 read", cmd_test_read_pcf8574),
	SHELL_SUBCMD_SET_END /* Array terminated. */
);
SHELL_CMD_REGISTER(pcf8574, &pcf8574_test, "pcf8574 driver", NULL);
