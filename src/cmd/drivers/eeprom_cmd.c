#include <zephyr.h>
#include <shell/shell.h>
#include <stdlib.h>
#include <drivers/eeprom.h>
const struct device *eeprom_dev=NULL;
const uint8_t wr_buf[4] = { 0xFF, 0xEE, 0xDD, 0xCC };
uint8_t rd_buf[sizeof(wr_buf)];

static int cmd_test_get_size_eeprom(const struct shell *shell, size_t argc, char **argv)
{
	size_t size;
	eeprom_dev = device_get_binding("EEPROM_0");
	if(eeprom_dev==NULL){
		shell_error(shell,"dev is Null");
		return -1;
	}
	size = eeprom_get_size(eeprom_dev);
	if(size==0){
        shell_error(shell,"get size is fail");
		return -1;
    }
    shell_info(shell,"eeprom size is:[%d]",(uint16_t)size);
	return 0;
}

static int cmd_test_write_eeprom(const struct shell *shell, size_t argc, char **argv)
{
	int err;
	eeprom_dev = device_get_binding("EEPROM_0");
	if(eeprom_dev==NULL){
		shell_error(shell,"dev is Null");
		return -1;
	}
	err=eeprom_write(eeprom_dev, 0, wr_buf, sizeof(wr_buf));
	if(err){
        shell_error(shell,"write is fail");
		return -1;
    }
	shell_info(shell,"write OK");
    shell_hexdump_line(shell,0,wr_buf, sizeof(wr_buf));
	return 0;
}

static int cmd_test_read_eeprom(const struct shell *shell, size_t argc, char **argv)
{
	int err;
	eeprom_dev = device_get_binding("EEPROM_0");
	if(eeprom_dev==NULL){
		shell_error(shell,"dev is Null");
		return -1;
	}
	err=eeprom_read(eeprom_dev, 0, rd_buf, sizeof(rd_buf));
	if(err){
        shell_error(shell,"read is fail");
		return -1;
    }
	shell_info(shell,"read OK");
    shell_hexdump_line(shell,0,rd_buf, sizeof(rd_buf));
	return 0;
}

SHELL_STATIC_SUBCMD_SET_CREATE(eeprom_test,
	SHELL_CMD(getsize, NULL, "eeprom getsize", cmd_test_get_size_eeprom),
	SHELL_CMD(write, NULL, "eeprom write", cmd_test_write_eeprom),
	SHELL_CMD(read, NULL, "eeprom read", cmd_test_read_eeprom),
	SHELL_SUBCMD_SET_END /* Array terminated. */
);
SHELL_CMD_REGISTER(eeprom_iic, &eeprom_test, "eeprom driver", NULL);
