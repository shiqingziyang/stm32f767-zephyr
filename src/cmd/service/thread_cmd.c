#include <zephyr.h>
#include <shell/shell.h>
#include <stdlib.h>
#include <logging/log.h>
#define LOG_MODULE_NAME service_cmd_thread
LOG_MODULE_REGISTER(LOG_MODULE_NAME, LOG_LEVEL_DBG);

static k_tid_t test_thread_id;
#define TEST_THREAD_SIZE 100
#define TEST_PRIORITY 10
static struct k_thread test_thread_data;
static struct k_work test_work_data;
K_THREAD_STACK_DEFINE(test_thread_stack_area, TEST_THREAD_SIZE);

static void test_thread(void *time_out,void *p2,void *p3)
{
    ARG_UNUSED(p2);
    ARG_UNUSED(p3);
    uint16_t timeout = *(uint16_t *)time_out;
    uint64_t timeout_clc=k_uptime_get()+timeout;
    while(k_uptime_get()<timeout_clc){
        LOG_INF("test work!\n");
        k_sleep(K_MSEC(500));
    }
}

static int cmd_thread_create(const struct shell *shell, size_t argc, char **argv)
{
    static uint16_t timeout=0;
    if(argc!=2){
        shell_error(shell, "cmd agr is [timeout(int)]");
        return -1;
    }
	timeout=atoi(argv[1]);
    //创建一个测试线程，超时间时间到了之后自动退出
    test_thread_id = k_thread_create(&test_thread_data, test_thread_stack_area,
                                     K_THREAD_STACK_SIZEOF(test_thread_stack_area),
                                     test_thread,
                                     (void *)&timeout, NULL, NULL,
                                     TEST_PRIORITY, 0, K_NO_WAIT);
	return 0;
}

static void test_work_handler(struct k_work *agr)
{
    ARG_UNUSED(agr);
    LOG_INF("work msg.\n");
}

static int cmd_work_create(const struct shell *shell, size_t argc, char **argv)
{
    if(argc>2){
        shell_error(shell, "no agr is cmd");
        return -1;
    }
	
    k_work_init(&test_work_data,test_work_handler);
    k_work_submit(&test_work_data);
	return 0;
}

SHELL_STATIC_SUBCMD_SET_CREATE(thread_test,
	SHELL_CMD(create, NULL, "create thread,abort timeout", cmd_thread_create),
	SHELL_CMD(work, NULL, "create work queue", cmd_work_create),
	SHELL_SUBCMD_SET_END /* Array terminated. */
);
SHELL_CMD_REGISTER(thread, &thread_test, "Thread commands", NULL);